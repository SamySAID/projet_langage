bison -d mini.y
rename mini.tab.h mini.h
rename mini.tab.c mini.y.c
flex mini.l
rename lex.yy.c mini.lex.c
gcc -c mini.lex.c -o mini.lex.o
gcc -c mini.y.c -o mini.y.o
gcc -o mini mini.lex.o mini.y.o
pause