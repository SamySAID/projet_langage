%{

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
double mem[26];
int yylineno;
%}

%union { 
	double val;
	char* var;
}

%token  <val> NOMBRE
%token  <var> VARIABLE
%token 	 INT DOUBLE STRING
%token   PLUS  MOINS FOIS  DIVISE  PUISSANCE   COMPARE_TO  DIFFERENT  EMPTY  INF  SUP INFEGAL  INFSUP
%token   WHILE  PRINT
%token   PARENTHESE_GAUCHE PARENTHESE_DROITE CROCHET_DROIT  CROCHET_GAUCHE
%token   FIN

%type   <val> Expression

%left PLUS  MOINS
%left FOIS  DIVISE
%left NEG
%left IF  DO ELSE ELSEIF ENDIF 
%right  PUISSANCE

%start Input
%%

Input:
    /* Vide */
  | Input Stmt
  ;

Stmt:
    FIN
  | Expression FIN    { printf("CALC : %f\nUSER : ",$1);  }
  | INT VARIABLE EGAL Expression FIN    { printf("Ecriture memoire\nCALC : %s vaut %f \nUSER : ",$2,$4); mem[$2[0]-'a']= (int)$3;}
  | DOUBLE VARIABLE EGAL Expression FIN    { printf("Ecriture memoire\nCALC : %s vaut %f \nUSER : ",$2,$4); mem[$2[0]-'a']= $3;}
  | STRING VARIABLE EGAL Expression FIN    { printf("Ecriture memoire\nCALC : %s vaut %f \nUSER : ",$2,$4); mem[$2[0]-'a']= $3;}
  | WhileStmt FIN
  | Condition FIN
  | IF PARENTHESE_GAUCHE Condition PARENTHESE_DROITE CROCHET_GAUCHE Expression CROCHET_DROIT FIN{ if($2){$$ = $4;}}
  | PRINT PARENTHESE_GAUCHE Expression PARENTHESE_DROITE FIN {printf("%s",$3);}
  ;

Expression:
    NOMBRE      { $$=$1;   }
  | VARIABLE { printf("Lecture memoire\n"); $$=mem[$1[0]-'a'] ;}
  | Expression PLUS Expression  { $$=$1+$3; }
  | Expression MOINS Expression { $$=$1-$3; }
  | Expression FOIS Expression  { $$=$1*$3; }
  | Expression DIVISE Expression  { $$=$1/$3; }
  | MOINS Expression %prec NEG  { $$=-$2; }
  | Expression PUISSANCE Expression { $$=pow($1,$3); }
  | PARENTHESE_GAUCHE Expression PARENTHESE_DROITE  { $$=$2; }
  ;
  
Condition :
  Expression COMPARE_TO Expression  { if($1 == $3){$$ = 1;}else{$$=0;}}
  | Expression DIFFERENT Expression{ if($1 != $3){$$ = 0;}else{$$=0;}}
  | Expression INF Expression{ if($1 < $3){$$ = 0;}else{$$=0;}}
  | Expression SUP Expression{ if($1 > $3){$$ = 0;}else{$$=0;}}
  | Expression INFEGAL Expression{ if($1 <= $3){$$ = 0;}else{$$=0;}}
  | Expression SUPEGAL  Expression{ if($1 >= $3){$$ = 0;}else{$$=0;}}
  ;

WhileStmt:
	WHILE PARENTHESE_GAUCHE Condition PARENTHESE_DROITE Stmt
   ;
%%

int yyerror(char *s) {
  printf(stderr, "line %d: %s\n", yylineno, s);
}

int main(void) {
printf( "cmd # ");
  yyparse();
}
