%option noyywrap
%{

#include "calc.h"
#include <stdlib.h>

%}

blancs    [ \t]+

chiffre   [0-9]
entier    {chiffre}+
exposant  [eE][+-]?{entier}

reel    {entier}("."{entier})?{exposant}?

variable  [a-zA-Z]+

%%

"if"  return(IF);
"do"  return(DO);
"elseif"  return(ELSEIF);
"else"  return(ELSE);
"endif"  return(ENDIF);

"=="  return(COMPARE_TO);
"!="  return(DIFFERENT);
"!"   return(EMPTY);
"<"   return(INF);
">"   return(SUP);

"while"  return(WHILE);
"print"  return(PRINT);

"int"  return INT;
"double"  return DOUBLE;
"string"  return STRING;

{blancs}  { /* On ignore */ }

{reel}    {
	yylval.val=atof(yytext);
	return(NOMBRE);
    }

{variable}    {
	yylval.var=(char*)_strdup(yytext);
	return(VARIABLE);
    }

"+"   return(PLUS);
"-"   return(MOINS);

"*"   return(FOIS);
"/"   return(DIVISE);

"^"   return(PUISSANCE);

"("   return(PARENTHESE_GAUCHE);
")"   return(PARENTHESE_DROITE);
"{"	  return(CROCHET_DROIT);
"}"   return(CROCHET_GAUCHE);
"="   return(EGAL);
"\n"  return(FIN);
